
class ErrorCode:
    NO_ERROR = 0
    ERROR = 1


class ReqType:
    PLAYER_ENTER = 0
    PLAYER_ENTER_CONFIRM = 1
    MANUAL_HIT = 2
    MANUAL_HIT_CONFIRM = 3
    AUTO_HIT_UPDATE = 4
    AUTO_HIT_UPDATE_CONFIRM = 5
    AUTO_HEAL = 6
    AUTO_HEAL_CONFIRM = 7
    AUTO_HEAL_UPDATE = 8
    AUTO_HEAL_UPDATE_CONFIRM = 9
    GET_DUNGEONS = 10
    GET_DUNGEONS_CONFIRM = 11
    ENTER_DUNGEON = 12
    ENTER_DUNGEON_CONFIRM = 13
    PLAYER_ALREADY_LOGGED_IN = 14