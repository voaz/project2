import sqlite3


class Database:
    _connection = sqlite3.connect('gametest.db')

    def get_player(self, user_token):
        try:
            cursor = self._connection.cursor()
            player = cursor.execute('SELECT * form players WHERE user_token = "%s"' % user_token)
        except sqlite3.Error:
            print('database error')
        return player

    def init_database(self):
        cursor = self._connection.cursor()
        cursor.execute('CREATE TABLE players(nickname text, lvl integer, strength integer, hp integer)')
        self._connection.commit()

if __name__ == '__main__':
    db = Database()
    # db.init_database()
    user_token_hash = abs(hash('5qwertyu')).to_bytes(20, 'big')[-4:]
    client_addr_hash = abs(hash('123.34.54.34:5323')).to_bytes(8, 'big')[-4:]
    print (user_token_hash)
    print(client_addr_hash)
    session_token = bytes()
    session_token = user_token_hash + client_addr_hash
    # session_token.extend(client_addr_hash)
    print(session_token)