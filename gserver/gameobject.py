class Unit:
    def __init__(self, hp, strength, name):
        self.HP = hp
        self.strength = strength
        self.name = name

    def hit(self) -> int:
        return self.strength


class Monster(Unit):
    pass


class MonsterManager:
    def __init__(self):
        pass


class Dungeon:
    def __init__(self, lvl, title, players_count, monster_count):
        self._lvl = lvl
        self._titile = title
        self._players_count = players_count
        self._monster_count = monster_count
        self._monsters = []
        for _ in range(0, monster_count):
            self._monsters.append(Monster(10, 1, 'Ratatulus'))
# TODO: Сюда добавятся поля: сколько игроков уже стоит на входе,
#  возможно какая награда ждет на выходе,
#  Квестовый данжн
# TODO: Рандомная генерация монстров

    def info(self) -> dict:
        data = {"lvl": self._lvl,
                "title": self._titile,
                "playerscount": self._players_count,
                "monsterscount": self._monster_count}
        return data

    def _generate_monsters(self, lvl, monster_type):
        pass


