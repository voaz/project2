from exceptions import *
from gameobject import Unit

class Player(Unit):
    def __init__(self, hp, strength, auto_hit_strength=0, heal=0):
        self.auto_hit_strength = auto_hit_strength
        self.heal = heal
        super(Player, self).__init__(hp, strength)


class PlayerManager:
    def __init__(self):
        self._connected_players = {} #{token: client_address}
        self._players_session_tokens = {} #{token: session_token}

    def user_connected(self, user_token, client_address):
        if user_token in self._connected_players:
            if client_address != self._connected_players[user_token]:
                # log that the user changed client_address
                print("user {} already logged in but changed his client_address".format(user_token))
            else:
                raise AlreadyLoggedInError

        self._connected_players[user_token] = client_address
        user_token_hash = abs(hash(user_token)).to_bytes(10, 'big')[-4:]
        client_addr_hash = abs(hash(client_address[0])).to_bytes(10, 'big')[-4:]
        print(user_token_hash)
        print(client_addr_hash)
        session_token = user_token_hash + client_addr_hash
        print('hash = ', session_token)
        self._players_session_tokens[session_token] = user_token
        # Load player.py stats from DB
        return session_token

    def get_lvl(self, session_token):
        return 1
