import logging
import socketserver
from codes import ReqType
from player import *
from gameobject import *
import json
import threading

log = logging.getLogger('udp_server')


class DungeonManager:
    _global_dungeons = []
    _personal_dungeons = {}  # session_token: [1 person private dungeons]
    _player_in_dungeon = {} # session_token: dungeon
    def __init__(self):
        pass

    def generate_personal_dungeon(self, session_token, lvl):
        random_monsters_count = 5
        dungeon = Dungeon(lvl, "Old Forest", 1, random_monsters_count)
        self._personal_dungeons[session_token] = [dungeon,]

    def get_dungeons(self, session_token, lvl):
        if session_token not in self._personal_dungeons:
            self.generate_personal_dungeon(session_token, lvl)
        return self._personal_dungeons[session_token] # + global_dungeons

    def player_enter_dungeon(self, session_token, dungeon_index):
        # dungeon = ''
        print("player enter")
        print("dungeon index = ", dungeon_index)
        if dungeon_index in range(0, len(self._personal_dungeons[session_token])):
            print("dungeon in range of personal dungeons")
            dungeon = self._personal_dungeons[session_token][dungeon_index]
            self._player_in_dungeon[session_token] = dungeon
            port = self.run_server()
            return port
        elif dungeon_index in range(len(self._personal_dungeons[session_token]), len(self._personal_dungeons[session_token]) + len(self._global_dungeons)):
            # dungeon =
            print("in range of public dun")
            pass





    def run_server(self):
        port = 1235 #TODO: check available port
        HOST, PORT = "localhost", port
        server = socketserver.UDPServer((HOST, PORT), DungeonHandler)

        server_thread = threading.Thread(target=server.serve_forever())
        server_thread.daemon = True
        server_thread.start()
        print("Server loop running in thread:", server_thread.name)
        return port





class DungeonHandler(socketserver.BaseRequestHandler):
    _player_client_address = {}
    _players = {} # token, client_address
    _events = []
    _monsterManager = MonsterManager()

    def handle(self):
        pass

    def handle_hit(self, player, damage):
        self._monsterManager.hit(player, damage)

    def handle_autohit_update(self, ):
        pass

    def notify_players(self):
        for player in self._players:
            self.sock.sendto()


class MainHandler(socketserver.BaseRequestHandler):
    _player_client_address = {}
    _players = {} # {token: client_address}
    _events = []
    _dungeon_manager = DungeonManager()
    _players_manager = PlayerManager()
    '''
        В зависимости от типа запроса выполнить действие
        Сохранить пользователя в 
    '''
    def handle(self):
        data = self.request[0]
        self.socket = self.request[1]
        print("received = {}".format(data))
        # self.socket.sendto(data, self.client_address)
        # self._players
        req_type = int(data[0])

        if req_type == ReqType.PLAYER_ENTER:
            self.handle_enter(data)

        elif req_type == ReqType.GET_DUNGEONS:
            self.handle_get_dungeons(data)

        elif req_type == ReqType.ENTER_DUNGEON:
            self.handle_enter_dungeon(data)


    def handle_enter(self, data):
        user_token = data[1:9]
        #NOTE: Читаем bp базы всех пользователей и определяем существование этого токена
        ret_data = bytearray()
        try:
            session_token = self._players_manager.user_connected(user_token, self.client_address)
            ret_data.append(ReqType.PLAYER_ENTER_CONFIRM)
            ret_data.extend(session_token)
        except AlreadyLoggedInError:
            print("user {} already logged in", user_token)
            ret_data.append(ReqType.PLAYER_ALREADY_LOGGED_IN)
        self.socket.sendto(ret_data, self.client_address)

    def handle_get_dungeons(self, data):
        session_token = data[1:9]
        dungeons = self._dungeon_manager.get_dungeons(session_token, self._players_manager.get_lvl(session_token))
        dun_json = json.JSONEncoder().encode([dungeon.info() for dungeon in dungeons])
        ret_data = bytearray()
        ret_data.append(ReqType.GET_DUNGEONS_CONFIRM)
        ret_data.extend(session_token)
        ret_data.extend(bytes(dun_json, 'utf-8'))
        self.socket.sendto(ret_data, self.client_address)

    def handle_enter_dungeon(self, data):
        session_token = data[1:9]
        dungeon_index = data[9:]
        dungeon_index = int.from_bytes(dungeon_index, "big")
        port = self._dungeon_manager.player_enter_dungeon(session_token, dungeon_index)
        ret_data = bytearray()
        ret_data.append(ReqType.ENTER_DUNGEON_CONFIRM)
        ret_data.extend(session_token)
        ret_data.append(port)
        self.socket.sendto(ret_data, self.client_address)


    def response_to_user(self, response_type, user_token, data):
        player_addr = ''
        try:
            player_addr = self._players[user_token]
        except KeyError:
            pass # NOTE: что-то сделать надо
        rdata = bytearray()
        rdata.extend(response_type)
        rdata.extend(bytes(data, "utf-8"))
        self.socket.sendto(rdata, player_addr)


if __name__ == "__main__":
    HOST, PORT = "localhost", 1234
    server = socketserver.UDPServer((HOST, PORT), MainHandler)
    server.serve_forever()

'''
- Сервер отсылает по евенту измененное состояние игры:
1. Если монстр произвел действие (удар)
2. Если игрок произвел действие (удар)

- Измененное состояние по эвенту отправляется всем игрокам

'''
'''
0:1 - request type
1:  - data
in battle
1:3 - received damage
3:5 - received heal

event
0:1 - request type
in battle
1:3 - received damage
3:5 - received heal
5:6  - dealed damage by user
6:  - dealed by others
'''

'''
1. user token, session token
user token выдается после прохождения регистрации
session token выдается сервером в результате передачи клиентом user token серверу
после получения

2.Events
- урон 

три массива:
урон, нанесенный врагу всеми игроками в рейде
урон, нанесенный игроку (для каждого игрока свой)
очки лечения, полученные хилами
'''

'''
На энтер данжен 
сервер создает экземпляр сокетсервера для и подключает хэндлер данженхэндлер 
клиенту возвращается номер порта, по которому можно войди в данжен
'''

'''
    MainHandler обрабатывает запросы:
    - вход пользователя
    - запрос на получение списка данженов
    - вход в данжн
'''
'''
    - При запросе на вход в данжн создается экземпляр сервера данжена
'''

'''
Этап 2
 - Сообщения клиенту об изменении состояния игры
 - Механика боя: урон от монстров

Этап 3
 
'''